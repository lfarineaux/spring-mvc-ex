<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.cda.dto.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<!DOCTYPE html>
<html>
<head>
<link href="static/css/style.css" rel="stylesheet">
<link href="static/fontawesome/css/all.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>

<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
			<div class="col col-lg-9 mt-1">
				<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
						DES PRODUITS - Ajouter un produit</strong>
				</a>
			</div>
			
		<div class="col col-lg-3 mt-1">
					<c:if test="${sessionScope.user.login!=null}">
						<p style="color: white">
							<b>Bienvenue <i>${sessionScope.user.login} !</i></b>
						</p>
					</c:if>
				</div>
			</div>

		</div>
	</header>
	
	<div class="card text-center container mt-5">
	
			<form style="border: solid 1px Gray; background-color: #e5e7e9; border-top-left-radius: 15px;
			 border-top-right-radius: 15px; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px;" method="post" action="addProduit">
				<h5><b><center>AJOUTER UN PRODUIT</center></b>	</h5>
				<label for="labelProd" class="col col-form-label"><b>Label : </b></label>
				<p>
					<input type="text" name="labelProd" required>
				</p>
				<label for="prixProd" class="col col-form-label"><b>Prix : </b></label>
				<p>
					<input type="text" name="prixProd"  required>
				</p>
				
				<label for="quantiteProd" class="col col-form-label"><b>Quantite : </b></label>
				<p>
					<input type="text" name="quantiteProd"  required>
				</p>
				<label for="categorieProd" class="col col-form-label"><b>Categorie : </b></label>
				<p>
					<select name ="categorieProd">
						<c:if test="${maListe != null}" >
						<c:forEach items="${maListe}" var="cat">
						<option value="${cat.id}">${cat.label}</option>
						</c:forEach>
						</c:if>
					</select>
				
				</p>
				
				<div class="container my-4">
				<div class="row justify-content-md-center">
					<div class="col col-lg-1 mt-1"></div>

					<div class="col col-lg-2 mt-1">
						<a class="btn btn-secondary" href="listProduit?page=0" role="button"><span
							class="badge badge-light"><i class="fas fa-window-close"></i></span>
							Annuler</a>
					</div>
					<div class="col col-lg-1 mt-1"></div>
					<div class="col col-lg-2 mt-1">
						<input class="btn btn-secondary" value="valider"
							type="submit" id="submit-btn" style="display: none">
						<div class="col-sm">
							<a class="btn btn-secondary" href="#" id="click-btn"
								role="button"><span class="badge badge-light"><i
									class="fas fa-check-square"></i></span> Valider</a>
						</div>
					</div>

				</div>
			</div>
			</form>
		</div>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
		<script src="static/js/script.js"></script>
</body>
</html>