<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.afpa.cda.dto.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>

<html>
<head>
<link href="static/css/style.css" rel="stylesheet">
<link href="static/fontawesome/css/all.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>


<body style="background-color: #eaf2f8; font-family: Tw Cen MT">
	<header>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
			<div class="col col-lg-9 mt-1">
				<a href="#" class="navbar-brand d-flex align-items-center"> <strong>GESTION
						DES PRODUITS - Utilisateur</strong>
				</a>
			</div>
			
		<div class="col col-lg-3 mt-1">
					<c:if test="${sessionScope.user.login!=null}">
						<p style="color: white">
							<b>Bienvenue <i>${sessionScope.user.nom} !</i></b>
						</p>
					</c:if>
				</div>
			</div>
		</div>
	</header>
	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col col-lg-8">

				<form
					style="border: solid 1px Gray; background-color: #e5e7e9; border-top-left-radius: 15px; border-top-right-radius: 15px; border-bottom-left-radius: 15px; border-bottom-right-radius: 15px;">
					<div class="form-group">
						<h5><b><center>VOIR UN UTILISATEUR</center></b>	</h5>
						<label for="idPers" class="col col-form-label"><b>Identifiant</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="idPers" value="${id}">
						</div>
					</div>
					<div class="form-group">
						<label for="nom" class="col col-form-label"><b>Nom</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="nom" value="${nom}">
						</div>
					</div>
					<div class="form-group">
						<label for="prenom" class="col col-form-label"><b>Prénom</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="prenom" value="${prenom}">
						</div>
					</div>
					<div class="form-group">
						<label for="role" class="col col-form-label"><b>Rôle</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="role" value="${role.type}">
						</div>
					</div>
					<div class="form-group">
						<label for="login" class="col col-form-label"><b>Login</b></label>
						<div class="col">
							<input type="text" readonly class="form-control-plaintext"
								id="login" value="${login}">
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>
	<br>

	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col  col-lg-4"></div>
			<div class="col  col-lg-8">
				<a class="btn btn-secondary" href="listUser?page=0" role="button"><span
					class="badge badge-light"><i class="fas fa-list-ol"></i></span>
					Retour vers la liste</a>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
	<script src="static/js/script.js"></script>
</body>
</html>