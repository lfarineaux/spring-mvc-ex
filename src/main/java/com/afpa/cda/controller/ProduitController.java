package com.afpa.cda.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.CategorieDto;
import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.dto.RoleDto;
import com.afpa.cda.dto.UserDto;
import com.afpa.cda.entity.Categorie;
import com.afpa.cda.entity.Role;
import com.afpa.cda.service.ICategorieService;
import com.afpa.cda.service.IProduitService;
import com.afpa.cda.service.IRoleService;
import com.afpa.cda.service.IUserService;
import com.afpa.cda.service.ProduitServiceImpl;
import com.afpa.cda.service.UserServiceImpl;

@Controller
public class ProduitController {

	@Autowired
	private IProduitService produitService;

	@Autowired
	ICategorieService categorieService;


	@RequestMapping(value="/addProduit")
	public ModelAndView initAddProd(ModelAndView mv) {
		List<CategorieDto> maListe = categorieService.findAllCategories(0);
		mv.addObject("maListe",maListe);
		mv.setViewName("addProd");
		return mv;
	}

	@RequestMapping(value="/addProduit",
			method =RequestMethod.POST,
			params = {"labelProd","prixProd","quantiteProd","categorieProd"})
	public void addProd(
			@RequestParam(value="labelProd") String label,
			@RequestParam(value="prixProd") String prix,
			@RequestParam(value="quantiteProd") String quantite,
			@RequestParam(value="categorieProd") String categorie,
			HttpServletResponse resp
			) throws IOException {
		int Idcategorie =0;
		try {
			Idcategorie = Integer.parseInt(categorie);
		}catch (Exception e) {
			e.getMessage();
		}

		this.produitService.ajouter(
				ProduitDto.builder()
				.label(label)
				.prix(prix)
				.quantite(quantite)
				.categorie(CategorieDto.builder().id(Idcategorie).build())
				.build());
		
		resp.sendRedirect("listProduit?page=0");

	}

	
@RequestMapping(value="/listProduit", method = RequestMethod.GET)
	public ModelAndView list
	(@RequestParam(value = "page", required = false) String pageStr, ModelAndView mv) {
		int page = 0;
		if(pageStr == null || pageStr.length() == 0) {

		} else {
			try {
				page = Integer.parseInt(pageStr.trim());
				if (page > ProduitServiceImpl.nbPage) {
					page = 0;
				}
			} catch (NumberFormatException e) {
				String message = "Erreur pas de lettre pour le numero de page";

			}
		}
		List<ProduitDto> listProduit = this.produitService.searchAllProduct(page);
		mv.addObject("pageN", page);
		mv.addObject("pageMax", UserServiceImpl.nbPage);
		mv.addObject("listProduit", listProduit);
		mv.setViewName("listProd");
		return mv;
	}
	
	
	@RequestMapping(value="/showProduit", method = RequestMethod.GET, params = {"id"})
	public ModelAndView initShow(@RequestParam(value = "id") String idStr, ModelAndView mv) {
		int id = 0;
		String message = null;
		ProduitDto produitDto = null;
		try {
			id = Integer.parseInt(idStr.trim());
			produitDto = this.produitService.findById(Integer.parseInt(idStr.trim()));
			System.out.println(produitDto);

		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		} catch (Exception e) {
			message = e.getMessage();
		}
		mv.addObject("id", id);
		mv.addObject("labelProd",produitDto.getLabel());
		mv.addObject("prixProd", produitDto.getPrix());
		mv.addObject("quantiteProd", produitDto.getQuantite());
		mv.addObject("categorieProd", produitDto.getCategorie().getLabel());
		mv.setViewName("showProd");
		return mv;
	}

	@RequestMapping(value="/updateProduit",
			method =RequestMethod.GET,params = {"id"})
	public ModelAndView initUpdate
	(@RequestParam(value = "id") String idStr,
			ModelAndView mv) {
		int id=0;
		String message=null;
		ProduitDto produitDto = null;
		try {
			id = Integer.parseInt(idStr.trim());
			produitDto =this.produitService.findById(id);
		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		}catch (Exception e) {
			message = e.getMessage();
		}

		mv.addObject("id", id);
		mv.addObject("oldLabelProd",produitDto.getLabel());
		mv.addObject("oldPrixProd", produitDto.getPrix());
		mv.addObject("oldQuantiteProd", produitDto.getQuantite());
		mv.addObject("oldCategorieProd", produitDto.getCategorie().getLabel());
		
		mv.setViewName("updateProd");
		return mv;
	}

	@RequestMapping(value="/updateProduit", method= RequestMethod.POST)
	public void update(@RequestParam(value = "id") String idStr, 
			@RequestParam(value="labelProd") String label,
			@RequestParam(value="prixProd") String prix,
			@RequestParam(value="quantiteProd") String quantite,
			@RequestParam(value="categorieProd") Categorie categorie,
			HttpServletResponse resp) throws IOException {
		int id = 0;
		try {
			id = Integer.parseInt(idStr.trim());

		} catch (NumberFormatException e) {
			String message = "Erreur pas de lettre pour l'id";
		}
		this.produitService.updateProduit(id, label, prix, quantite, categorie);
		resp.sendRedirect("listProduit?page=0");
	}


	@RequestMapping(value="/deleteProduit",
			method=RequestMethod.GET,params = {"id"})
	public void delete(
			@RequestParam(value = "id") String idStr,
			HttpServletResponse resp) throws IOException {
		int id = 0;
		try {
			id = Integer.parseInt(idStr.trim());
		} catch (NumberFormatException e) {
			String message = "Erreur pas de lettre pour l'id";
		}
		this.produitService.deleteById(id);
		resp.sendRedirect("listProduit?page=0");
	}

}
