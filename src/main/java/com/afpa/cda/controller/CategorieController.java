package com.afpa.cda.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.CategorieDto;
import com.afpa.cda.service.CategorieServiceImpl;
import com.afpa.cda.service.ICategorieService;

@Controller
public class CategorieController {

	@Autowired
	private ICategorieService categorieService;
	
	@RequestMapping(value = "/addCategorie")
	public ModelAndView initAdd(ModelAndView mv) {
		mv.setViewName("addCat");
		return mv;
	}
	
	@RequestMapping(value = "/addCategorie", 
			method =RequestMethod.POST,
			params = {"labelCategorie" })
	public void add(
			@RequestParam(value = "labelCategorie")String labelC,
			HttpServletResponse resp,
			ModelAndView mv) throws IOException {
		this.categorieService.AddCategorie(
				CategorieDto.builder()
				.label(labelC)
				.build());
		resp.sendRedirect("listCategorie?page=0");
	}
	
	@RequestMapping(value = "/listCategorie", method =RequestMethod.GET)
	public ModelAndView list(@RequestParam(value = "page", required = false) String pageStr,
			ModelAndView mv) {
		
		int page = 0;
		if (pageStr == null || pageStr.length() == 0) {
			
		}else {
			try {
				page=Integer.parseInt(pageStr.trim());
				if (page > CategorieServiceImpl.nbPage) {
					page=0;
				}
				
			} catch (NumberFormatException e) {
				String message ="Erreur";
			}
		}
		List<CategorieDto> listCat = this.categorieService.findAllCategories(page);
		mv.addObject("pageN", page);
		mv.addObject("pageMax", CategorieServiceImpl.nbPage);
		mv.addObject("listCategorie", listCat);
		mv.setViewName("listCat");
				return mv;
	}
	@RequestMapping(value = "/showCategorie",
			method =RequestMethod.GET,params = {"id"})
	public ModelAndView initShow
	(@RequestParam(value = "id") String idStr,
			ModelAndView mv) {
		
		
		int id=0;
		String message=null;
		String labelC =null;
		try {
			id = Integer.parseInt(idStr.trim());
			labelC =this.categorieService.findLabelById(id);

		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		} catch (Exception e) {
			message = e.getMessage();
		}

		mv.addObject("labelC",labelC);
		mv.addObject("id",id);
		
		mv.setViewName("showCat");
		return mv;
	}
	
	@RequestMapping(value = "/updateCategorie",
			method =RequestMethod.GET,params = {"id"})
	public ModelAndView  initUpdate(@RequestParam(value = "id") String idStr,
			ModelAndView mv) {
		
		int id=0;
		String message=null;
		String labelC =null;
		try {
			id = Integer.parseInt(idStr.trim());
			labelC =this.categorieService.findLabelById(id);
		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		}catch (Exception e) {
			message = e.getMessage();
		}

		mv.addObject("labelC",labelC);
		mv.addObject("id",id);
		
		mv.setViewName("updateCat");
		return mv;
	}
	
	@RequestMapping(value="/updateCategorie",
			method =RequestMethod.POST)
	public void Update(
			@RequestParam(value = "id") String idStr,
			@RequestParam(value = "labelCategorie") String labelC,
			HttpServletResponse resp) throws IOException {
		int id = 0;
		try {
			id = Integer.parseInt(idStr.trim());
		} catch (NumberFormatException e) {
			String message = "Erreur pas de lettre pour l'id";
		}
		this.categorieService.updateCategorie(labelC, id);
		resp.sendRedirect("listCategorie?page=0");
		
	}
	@RequestMapping(value="/deleteCategorie",
			method =RequestMethod.GET,params = {"id"})
	public void delete(@RequestParam(value = "id") String idStr,
			HttpServletResponse resp) throws IOException {

		int id=0;
		String message=null;
		try {
			id = Integer.parseInt(idStr.trim());
			this.categorieService.deleteById(id);

		} catch (NumberFormatException e) {
			message = "Erreur pas de lettre pour l'id";
		} catch (Exception e) {
			message = e.getMessage();
		}

		resp.sendRedirect("listCategorie");

	}
	
}
