package com.afpa.cda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.afpa.cda.service.IRoleService;


@Controller
public class MenuController {


	@RequestMapping(value="/index")
	public ModelAndView initMenu(ModelAndView mv) {

		mv.setViewName("Menu");
		return mv;
	}

	@RequestMapping(value="/")
	public ModelAndView Menu(ModelAndView mv) {

		mv.setViewName("Menu");
		return mv;
	}

}
