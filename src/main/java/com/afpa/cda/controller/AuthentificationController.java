package com.afpa.cda.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.UserDto;
import com.afpa.cda.service.IUserService;
import com.afpa.cda.service.UserServiceImpl;

@Controller
public class AuthentificationController {
	@Autowired
	private IUserService userserv;
	
	
	
	@RequestMapping(value="/connexion")
	public ModelAndView pageConnexion(ModelAndView mv) {
		mv.setViewName("login");
		return mv;
	}
	
	
	//Connexion via formulaire en utilisant la m�thode POST
	@RequestMapping(value="/connexionControle",method = RequestMethod.POST,
			params = {"identifiant","pass"}) //Les param�tres sont l'identifiant et le mot de passe
	public void controleDeConnexion(
			@RequestParam(value = "identifiant") String identifiant,
			@RequestParam(value = "pass") String pass,
			HttpServletRequest request,
			HttpServletResponse response,
			ModelAndView mv
			) throws IOException, ServletException {
		boolean vrai = false;
		HttpSession session = request.getSession();
		for (int i = 0;i<=UserServiceImpl.nbPage;i++) {
			List<UserDto> maListUser = this.userserv.chercherToutesLesPersonnes(i);
			for (UserDto userDto : maListUser) {
				System.out.println(userDto);
				if(userDto.getLogin().equals(identifiant ) && userDto.getPwd().equals(pass)) {
					session.setAttribute("userLog", userDto);
					vrai = true;
					response.sendRedirect("index");
				}
			}
		}
		
		if (!vrai) {
			request.setAttribute("msg", "Connection echou�");
			request.getServletContext().getRequestDispatcher("/connexion").forward(request,response);
		}
	}
	
	@RequestMapping(value="/deconnexion",method = RequestMethod.GET)
	public void controleurDeconnection(HttpServletRequest request,HttpServletResponse res) throws IOException {
		request.getSession().invalidate();
		res.sendRedirect("index");
		
	}
	
	
}
