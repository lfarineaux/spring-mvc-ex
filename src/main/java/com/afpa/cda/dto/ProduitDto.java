package com.afpa.cda.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProduitDto {

	
	
	private int code ;
	private String label;
	private String prix;
	private String quantite;
	private String img;
	private CategorieDto categorie;

	
	
}
