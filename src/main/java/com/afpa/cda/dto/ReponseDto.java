package com.afpa.cda.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class ReponseDto {
	private ReponseStatut status;
	private String msg;
}
