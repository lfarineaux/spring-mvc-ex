package com.afpa.cda.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// Classe pour pointer vers le dossier static qui contient les dossiers bootstrap, css, fontawesome, jquery et js


@Configuration
@EnableWebMvc
public class MvcConfig implements WebMvcConfigurer {

	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
          .addResourceHandler("/static/**")
          .addResourceLocations("/static/");
        registry
        .addResourceHandler("/img/**")
        .addResourceLocations("/img/");
    }
}
