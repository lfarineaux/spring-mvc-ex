package com.afpa.cda.service;

import java.util.List;

import com.afpa.cda.dto.RoleDto;
import com.afpa.cda.dto.UserDto;

public interface IUserService {
	
	public boolean deleteById(int id);

	public UserDto findById(int id);

	public Integer ajouter(UserDto build);

	List<UserDto>chercherToutesLesPersonnes(int page);
	
	void updateUser(int id, String nom, String prenom, String login, String pwd, RoleDto role);
}
