package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.IProduitDao;
import com.afpa.cda.dto.CategorieDto;
import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.entity.Categorie;
import com.afpa.cda.entity.Produit;
import com.afpa.cda.entity.Role;
import com.afpa.cda.entity.Utilisateur;

@Service
public class ProduitServiceImpl implements IProduitService{



	@Autowired
	private IProduitDao produitDao;

	@Autowired
	private ModelMapper modelMapper;

	public static final int INC=5;
	public static int fin ; 
	public static int nbPage;

	@Override
	public boolean deleteById(int id) {
		if(this.produitDao.existsById(id)) {
			this.produitDao.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public ProduitDto findById(int id) {
		Optional<Produit> prod = this.produitDao.findById(id);
		ProduitDto prodDto = null;
		if(prod.isPresent()) {
			Produit p = prod.get();
			System.out.println("++++++++++++++++++++++++++++"+  p);
			prodDto = new ProduitDto().builder()
					.code(p.getCode())
					.label(p.getLabel())
					.prix(p.getPrix())
					.quantite(p.getQuantite())
					.categorie(new CategorieDto().builder().id(p.getCategorie().getId()).label(p.getCategorie().getLabel()).build())
					.build();
		}
		
		System.out.println("badrane=================== "+prodDto);
		return prodDto;
	}
	
	@Override
	public Integer ajouter(ProduitDto build) {
		Produit p = this.modelMapper.map(build,Produit.class);
		p = this.produitDao.save(p);
		return p.getCode();

	}

	
	@Override
	public List<ProduitDto> searchAllProduct(int page) {
		Pageable pageable = PageRequest.of(page, INC);
		nbPage = this.produitDao.findAll(pageable).getTotalPages();	
		List<ProduitDto> maliste = this.produitDao.findAll(pageable)
				.stream()
				.map(e->ProduitDto.builder()
						.code(e.getCode())
						.label(e.getLabel())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
						.categorie(new CategorieDto().builder().id(e.getCategorie().getId()).label(e.getCategorie().getLabel()).build())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}
	
//	@Override
//	public List<ProduitDto> getpRODUIT(String chaine) {
//		// TODO Auto-generated method stub
//		return null;
//	}

//	@Override
//	public String findTypeById(int id) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public void updateProduit(int id, String label, String prix, String quantite, Categorie categorie)  {
		Optional<Produit> prod = this.produitDao.findById(id);
		if (prod.isPresent()) {
			Produit p = prod.get();
			p.setLabel(label);
			p.setPrix(prix);
			p.setQuantite(quantite);
			p.setCategorie(categorie);
			this.produitDao.save(p);

		}
	}
}
