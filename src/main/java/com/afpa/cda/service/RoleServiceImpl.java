package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.IRoleDao;
import com.afpa.cda.dto.RoleDto;
import com.afpa.cda.entity.Role;

@Service
public class RoleServiceImpl implements IRoleService {

	@Autowired
	private IRoleDao roleRepository;

	@Autowired
	private ModelMapper modelMapper;

	public static final int INC=5;
	public static int fin ; 
	public static int nbPage;


	@Override
	public List<RoleDto> chercherTousLesRoles(int page) {

		Pageable pageable = PageRequest.of(page, INC);
		nbPage = this.roleRepository.findAll(pageable).getTotalPages();

		List<RoleDto> maliste = this.roleRepository.findAll(pageable)
				.stream()
				.map(e->RoleDto.builder()
						.id(e.getId())
						.type(e.getType())
						.build())
				.collect(Collectors.toList());

		fin = maliste.size();
		return maliste;
	}

	@Override
	public boolean deleteById(int id) {
		if(this.roleRepository.existsById(id)) {
			this.roleRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public Optional<RoleDto> findById(int id) {
		Optional<Role> role = this.roleRepository.findById(id);
		
		Optional<RoleDto> res = Optional.empty();
		if(role.isPresent()) {
			Role r = role.get();
			RoleDto roleDto = this.modelMapper.map(r, RoleDto.class);
			res = Optional.of(roleDto);
					}
		return res;
	}

	@Override
	public String findTypeById(int id) {
		Optional<Role> role = this.roleRepository.findById(id);
		
		Role r = new Role();
		String typeR=null;
		if(role.isPresent()) {
			r = role.get();
			typeR = r.getType();
					}
		return typeR;
	}
	
	
	
	@Override
	public Integer ajouterRole(RoleDto role) {
		Role r = this.modelMapper.map(role,Role.class);
		r = this.roleRepository.save(r);
		return r.getId();

	}


	@Override
	public void updateRole(String type, int id) {
		Optional<Role> role = this.roleRepository.findById(id);
		if(role.isPresent()) {
			Role r = role.get();
			r.setType(type);
			this.roleRepository.save(r);
		}

	}
}
