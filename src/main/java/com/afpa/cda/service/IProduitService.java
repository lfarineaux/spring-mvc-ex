package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;

import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.dto.RoleDto;
import com.afpa.cda.entity.Categorie;
import com.afpa.cda.entity.Produit;

public interface IProduitService {

	
	public boolean deleteById(int id);
	
	public ProduitDto findById(int id);

	public Integer ajouter(ProduitDto build);

	List<ProduitDto> searchAllProduct(int page);
	
//	public List<ProduitDto> getpRODUIT(String chaine);

//	public String findTypeById(int id);


	void updateProduit(int id, String label, String prix, String quantite, Categorie categorie);

	
}
