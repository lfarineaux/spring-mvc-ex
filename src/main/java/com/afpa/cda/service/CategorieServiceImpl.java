package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.ICategorieDao;
import com.afpa.cda.dto.CategorieDto;
import com.afpa.cda.entity.Categorie;
import com.afpa.cda.entity.Role;

@Service
public class CategorieServiceImpl implements ICategorieService{

	@Autowired
	private ICategorieDao categorieRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public static final int INC=5;
	public static int fin;
	public static int nbPage;
	
	@Override
	public Optional<CategorieDto> findById(int id) {
		Optional<Categorie> categorie = this.categorieRepository.findById(id);
		Optional<CategorieDto> ctg = Optional.empty();
		if (categorie.isPresent()) {
			Categorie c = categorie.get();
			CategorieDto categorieDto = this.modelMapper.map(c, CategorieDto.class);
			ctg = Optional.of(categorieDto);
		}
		return ctg;
	}

	@Override
	public List<CategorieDto> findAllCategories(int page) {
		Pageable pageable = PageRequest.of(page, INC);
		nbPage = this.categorieRepository.findAll(pageable).getTotalPages();
		List<CategorieDto> listes = this.categorieRepository.findAll(pageable).stream()
				.map(e->CategorieDto.builder()
						.id(e.getId())
						.label(e.getLabel())
						.build())
				.collect(Collectors.toList());
		fin = listes.size();
		return listes;
	}

	@Override
	public Integer AddCategorie(CategorieDto categorie) {
		Categorie c = this.modelMapper.map(categorie,Categorie.class);
		c = this.categorieRepository.save(c);
		return c.getId();
	}

	@Override
	public void updateCategorie(String label, int id) {
		Optional<Categorie> categorie =this.categorieRepository.findById(id);
		if (categorie.isPresent()) {
			Categorie c = categorie.get();
			c.setLabel(label);
			c = this.categorieRepository.save(c);
		}
	}

	@Override
	public boolean deleteById(int id) {
		if (this.categorieRepository.existsById(id)) {
			this.categorieRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public String findLabelById(int id) {
Optional<Categorie> categorie = this.categorieRepository.findById(id);
		
Categorie r = new Categorie();
		String labelC=null;
		if(categorie.isPresent()) {
			r = categorie.get();
			labelC = r.getLabel();
					}
		return labelC;
	}

}
