package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;

import com.afpa.cda.dto.RoleDto;

public interface IRoleService {
	
	public boolean deleteById(int id);

	public Optional<RoleDto> findById(int id);

	List<RoleDto>chercherTousLesRoles(int page);

	Integer ajouterRole(RoleDto role);

	void updateRole(String type, int id);

	String findTypeById(int id);
	
}
