package com.afpa.cda.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Categorie;

@Repository
public interface ICategorieDao extends PagingAndSortingRepository<Categorie, Integer>{
	@Query("select c from Categorie c")
	public Page<Categorie> findAll(Pageable pageable);

}
