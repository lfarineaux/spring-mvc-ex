package com.afpa.cda.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Role;

@Repository
public interface IRoleDao extends PagingAndSortingRepository<Role, Integer> {
	@Query("select r from Role r")
	public Page<Role> findAll(Pageable pageable);
}
